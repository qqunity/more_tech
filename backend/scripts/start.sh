#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

python manage.py migrate
python manage.py collectstatic --noinput --verbosity 0
if [[ $PROD -eq 0 ]]
then
  python manage.py runserver 0.0.0.0:8002
else
  gunicorn chat.wsgi -b 0.0.0.0:8002 --chdir=/app
fi
