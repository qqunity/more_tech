from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.http import urlencode

from base.models import Profile, Post, Answer, Event, Question, Fact, Definition, Hint


class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = [
        'id',
        'user',
        'balance',
        'passive_income',
        'passive_outcome',
        'view_posts_link',
        'current_difficult',
        'tags',
        'age',
        'created_at',
        'updated_at'
    ]
    list_display_links = [
        'id',
        'user'
    ]

    def view_posts_link(self, obj):
        count = obj.posts.count()
        url = reverse('admin:base_post_changelist') + '?' + urlencode({'profiles__id': f'{obj.id}'})
        return format_html(f'<a href="{url}"> {count} posts</a>')
    view_posts_link.short_description = 'Посты'


class PostAdmin(admin.ModelAdmin):
    model = Post
    list_display = [
        'id',
        'title',
        'url',
        'created_at',
        'updated_at'
    ]
    list_display_links = [
        'id'
    ]


class AnswerAdmin(admin.ModelAdmin):
    model = Answer
    list_display = [
        'id',
        'profile',
        'answer',
        'event',
        'current_balance',
        'created_at',
        'updated_at'
    ]
    list_display_links = [
        'id',
        'profile',
        'event'
    ]


class EventAdmin(admin.ModelAdmin):
    model = Event
    list_display = [
        'id',
        'type',
        'title',
        'question',
        'view_hints_link',
        'passive_income',
        'passive_outcome',
        'income',
        'difficult',
        'tags',
        'outcome',
        'updated_at'
    ]
    list_display_links = [
        'id'
    ]

    def view_hints_link(self, obj):
        count = obj.hints.count()
        url = reverse('admin:base_hint_changelist') + '?' + urlencode({'events__id': f'{obj.id}'})
        return format_html(f'<a href="{url}"> {count} hints</a>')
    view_hints_link.short_description = 'Посты'


class QuestionAdmin(admin.ModelAdmin):
    model = Question
    list_display = [
        'id',
        'content',
        'left_answer',
        'view_facts_link',
        'view_definitions_link',
        'right_answer',
        'created_at',
        'updated_at'
    ]
    list_display_links = [
        'id'
    ]

    def view_facts_link(self, obj):
        count = obj.facts.count()
        url = reverse('admin:base_fact_changelist') + '?' + urlencode({'questions__id': f'{obj.id}'})
        return format_html(f'<a href="{url}"> {count} hints</a>')
    view_facts_link.short_description = 'Подсказки'

    def view_definitions_link(self, obj):
        count = obj.definitions.count()
        url = reverse('admin:base_definition_changelist') + '?' + urlencode({'questions__id': f'{obj.id}'})
        return format_html(f'<a href="{url}"> {count} definitions</a>')
    view_definitions_link.short_description = 'Определения'


class FactAdmin(admin.ModelAdmin):
    model = Fact
    list_display = [
        'id',
        'content',
        'coefficient',
        'object_of_influence',
        'created_at',
        'updated_at'
    ]
    list_display_links = [
        'id'
    ]


class DefinitionAdmin(admin.ModelAdmin):
    model = Definition
    list_display = [
        'id',
        'content',
        'created_at',
        'updated_at'
    ]
    list_display_links = [
        'id'
    ]


class HintAdmin(admin.ModelAdmin):
    model = Hint
    list_display = [
        'id',
        'content',
        'priority',
        'created_at',
        'updated_at'
    ]
    list_display_links = [
        'id'
    ]


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Fact, FactAdmin)
admin.site.register(Definition, DefinitionAdmin)
admin.site.register(Hint, HintAdmin)
