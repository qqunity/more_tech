import django


from base.choices import AnswerType, EventType, ObjectOfInfluenceType, GameEventType
from django.contrib.auth.models import User
from django.db import models


class Hint(models.Model):
    content = models.TextField(verbose_name='Содержание')
    priority = models.IntegerField()
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Подсказка #{self.pk}'


class Definition(models.Model):
    content = models.TextField(verbose_name='Содержание')
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Определение #{self.pk}'


class Fact(models.Model):
    content = models.TextField(verbose_name='Содержание')
    coefficient = models.FloatField(verbose_name='Коэффициент влияния')
    object_of_influence = models.CharField(max_length=1, choices=ObjectOfInfluenceType.choices, verbose_name='Объект влияния')
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Факт #{self.pk}'


class Question(models.Model):
    content = models.TextField(verbose_name='Содержание')
    facts = models.ManyToManyField(Fact, related_name='questions', verbose_name='Факты')
    answer = models.CharField(max_length=5, choices=AnswerType.choices, verbose_name='Правильный ответ')
    definitions = models.ManyToManyField(Definition, related_name='questions', verbose_name='Определения в содержании')
    left_answer = models.CharField(max_length=250, verbose_name='Текст левого ответа')
    right_answer = models.CharField(max_length=250, verbose_name='Текст правого ответа')
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Вопрос #{self.pk}'


class Event(models.Model):
    type = models.CharField(max_length=1, choices=EventType.choices)
    game_type = models.CharField(max_length=1, choices=GameEventType.choices, default=GameEventType.FREE)
    title = models.CharField(max_length=500)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='events', verbose_name='Вопрос')
    passive_income = models.FloatField(verbose_name='Пассивный доход', default=0.0)
    passive_outcome = models.FloatField(verbose_name='Пассивный рассход', default=0.0)
    income = models.FloatField(verbose_name='Доход', default=0.0)
    outcome = models.FloatField(verbose_name='Рассход', default=0.0)
    hints = models.ManyToManyField(Hint, related_name='events', verbose_name='Подсказки')
    difficult = models.IntegerField(verbose_name='Сложность')
    tags = models.TextField(verbose_name='Теги')
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Ивент #{self.pk}'


class Answer(models.Model):
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE, related_name='answers', verbose_name='Профиль')
    answer = models.CharField(max_length=5, choices=AnswerType.choices)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='answers', verbose_name='Ивент')
    current_balance = models.FloatField(verbose_name='Баланс после ответа')
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Ответ #{self.pk}'


class Post(models.Model):
    title = models.CharField(max_length=250, verbose_name='Название')
    url = models.CharField(max_length=300, verbose_name='Ссылка')
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Пост #{self.pk}'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile', verbose_name='Пользователь')
    balance = models.FloatField(verbose_name='Баланс')
    passive_income = models.FloatField(verbose_name='Пассивный доход', default=0.0)
    passive_outcome = models.FloatField(verbose_name='Пассивный рассход', default=0.0)
    posts = models.ManyToManyField(Post, related_name='profiles', verbose_name='Посты')
    current_difficult = models.IntegerField(verbose_name='Текущая сложность')
    tags = models.TextField(verbose_name='Теги', default='')
    age = models.IntegerField(verbose_name='Возраст', default=0)
    created_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время создания')
    updated_at = models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время обновления')

    def __str__(self):
        return f'Профиль #{self.pk}'
