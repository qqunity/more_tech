from django.db.models import Q
from rest_framework import status
from random import choice
from rest_framework.authtoken.admin import User
from rest_framework.response import Response
from base.serializers import UserSerializer, RigisterSerializer, EventSerializer, CreateAnswerSerializer, AnswerSerializer
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView
from base.models import Event, Answer
from base.filters import EventFilter
from django_filters.rest_framework import DjangoFilterBackend


class UserListView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]


class UserDetailView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]


class MeView(GenericAPIView):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return Response(UserSerializer(request.user).data, status=status.HTTP_200_OK)


class RegisterView(GenericAPIView):
    serializer_class = RigisterSerializer
    permission_classes = [IsAdminUser]

    def post(self, request):
        serializer = RigisterSerializer(data=request.data)
        if serializer.is_valid():
            return Response(serializer.create(validated_data=request.data), status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)


class EventView(ListAPIView):
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_class = EventFilter
    queryset = Event.objects.all()


class RandomEventView(GenericAPIView):
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_class = EventFilter

    def get(self, request):
        existing_identifiers = [answer.event.pk for answer in request.user.profile.answers.all()]
        return Response(EventSerializer(choice(self.filter_queryset(Event.objects.filter(difficult=request.user.profile.current_difficult).exclude(pk__in=existing_identifiers)))).data if self.filter_queryset(Event.objects.filter(difficult=request.user.profile.current_difficult).exclude(pk__in=existing_identifiers)).exists() else {}, status=status.HTTP_200_OK)


class AnswerView(ListAPIView):
    serializer_class = AnswerSerializer
    permission_classes = [IsAuthenticated]
    queryset = Answer.objects.all()

    def post(self, request):
        serializer = CreateAnswerSerializer(data=request.data)
        if serializer.is_valid():
            return Response(serializer.create(validated_data=request.data), status=status.HTTP_201_CREATED)
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class AnswerDetailView(RetrieveAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = [IsAuthenticated]
