from django.utils.translation import gettext_lazy as _
from django.db import models


class AnswerType(models.TextChoices):
    LEFT = 'left', _('left_answer')
    RIGHT = 'right', _('right_answer')


class EventType(models.TextChoices):
    REGULAR = '1', _('regular')
    RANDOM = '2', _('random')
    CRITICAL = '3', _('critical')


class GameEventType(models.TextChoices):
    STORY = '1', _('story')
    EDUCATION = '2', _('education')
    FREE = '3', _('free')


class ObjectOfInfluenceType(models.TextChoices):
    BALANCE = '1', _('balance')
    PASSIVE_INCOME = '2', _('passive_income')
    PASSIVE_OUTCOME = '3', _('passive_outcome')
