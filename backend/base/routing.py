from django.urls import path
from base.views import UserListView, UserDetailView, RegisterView, MeView, EventView, RandomEventView, AnswerView, AnswerDetailView

urlpatterns = [
    path('users/', UserListView.as_view()),
    path('users/<int:pk>/', UserDetailView.as_view()),
    path('register/', RegisterView.as_view()),
    path('me/', MeView.as_view()),
    path('events/', EventView.as_view()),
    path('events/random/', RandomEventView.as_view()),
    path('answers/', AnswerView.as_view()),
    path('answers/<int:pk>/', AnswerDetailView.as_view()),
]
