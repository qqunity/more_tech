from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from base.models import Event, Question, Profile, Definition, Fact, Hint, Post, Answer


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = [
            'id',
            'title',
            'url',
            'created_at',
            'updated_at'
        ]


class ProfileSerializer(serializers.ModelSerializer):
    posts = PostSerializer(read_only=True, many=True)

    class Meta:
        model = Profile
        fields = [
            'id',
            'balance',
            'passive_income',
            'passive_outcome',
            'current_difficult',
            'posts',
            'age',
            'tags',
            'created_at',
            'updated_at'
        ]


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'profile'
        ]


class RigisterSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    email = serializers.CharField(required=False)
    password = serializers.CharField(required=True)
    is_staff = serializers.BooleanField(required=True)
    profile = ProfileSerializer(required=True)

    def validate(self, data):
        if User.objects.filter(username=data['username']).exists():
            raise ValidationError(detail='Duplicate key value violates unique constraint "auth_user_username_key"', code=400)
        return data

    def create(self, validated_data):
        user_instance = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'] if 'email' in validated_data else validated_data['username'] + '@mail.ru',
            is_staff=validated_data['is_staff'] if 'is_staff' in validated_data else False
        )
        user_instance.set_password(validated_data['password'])
        user_instance.save()
        profile_instance = Profile.objects.create(**validated_data['profile'], user_id=user_instance.pk)
        return {
            'user_id': user_instance.id,
            'profile_id': profile_instance.id
        }


class HintSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hint
        fields = [
            'content',
            'priority',
            'created_at',
            'updated_at'
        ]


class DefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Definition
        fields = [
            'content',
            'created_at',
            'updated_at'
        ]


class FactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fact
        fields = [
            'content',
            'coefficient',
            'object_of_influence',
            'created_at',
            'updated_at'
        ]


class QuestionSerializer(serializers.ModelSerializer):
    facts = FactSerializer(read_only=True, many=True)
    definitions = DefinitionSerializer(read_only=True, many=True)

    class Meta:
        model = Question
        fields = [
            'id',
            'content',
            'facts',
            'answer',
            'definitions',
            'left_answer',
            'right_answer',
            'created_at',
            'updated_at'
        ]


class EventSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(read_only=True, many=False)
    hints = HintSerializer(read_only=True, many=True)

    class Meta:
        model = Event
        fields = [
            'id',
            'type',
            'game_type',
            'title',
            'question',
            'passive_income',
            'passive_outcome',
            'income',
            'outcome',
            'hints',
            'difficult',
            'tags',
            'created_at',
            'updated_at'
        ]


class AnswerSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True, many=False)
    event = EventSerializer(read_only=True, many=False)

    class Meta:
        model = Answer
        fields = [
            'id',
            'profile',
            'answer',
            'event',
            'current_balance',
            'created_at',
            'updated_at'
        ]


class CreateAnswerSerializer(serializers.ModelSerializer):
    profile_id = serializers.IntegerField(required=True)
    event_id = serializers.IntegerField(required=True)

    class Meta:
        model = Answer
        fields = [
            'id',
            'profile_id',
            'answer',
            'event_id',
            'current_balance',
            'created_at',
            'updated_at'
        ]

    def validate(self, data):
        if not Profile.objects.filter(pk=data['profile_id']).exists():
            raise ValidationError(detail='Profile does not exist', code=400)
        if not Event.objects.filter(pk=data['event_id']).exists():
            raise ValidationError(detail='Event does not exist', code=400)
        return data

    def create(self, validated_data):
        answer_instance = Answer(
            profile_id=validated_data['profile_id'],
            answer=validated_data['answer'],
            event_id=validated_data['event_id'],
            current_balance=validated_data['current_balance']
        )
        answer_instance.save()
        return {
            'answer_id': answer_instance.id
        }
