import django_filters


from base.choices import GameEventType


class EventFilter(django_filters.FilterSet):
    game_type = django_filters.ChoiceFilter(
        method='game_type_filter',
        field_name='game_type',
        choices=GameEventType.choices
    )


    def game_type_filter(self, queryset, name, game_type):
        return queryset.filter(game_type=game_type)
