from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view
from base.routing import urlpatterns as base_urlpatterns


swagger = get_swagger_view(title='More Tech API')

api_urlpatterns = [
    path('swagger/', swagger),
    path('', include(base_urlpatterns))
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urlpatterns)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]


